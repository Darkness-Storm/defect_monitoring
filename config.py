import os
# from peewee import SqliteDatabase
import configparser


basedir = os.path.abspath(os.path.dirname(__file__))
ROLE_ID = 50


def check_config():
    if not os.path.exists(os.path.join(basedir, 'config.ini')):
        set_default_config()
    return get_config()


def set_default_config():
    config = configparser.ConfigParser()
    config['DEFAULT'] = {}
    config['DEFAULT']['path_db'] = basedir
    config['DEFAULT']['name_db'] = 'defect.sqlite'
    write_ini_file(config)


def write_ini_file(config):
    with open('config.ini', 'w') as configfile:
        config.write(configfile)


def get_path_db():
    config = configparser.ConfigParser()
    config.read('config.ini')
    return config['DEFAULT']['path_db']


def get_name_db():
    config = configparser.ConfigParser()
    config.read('config.ini')
    return config['DEFAULT']['name_db']


def get_config():
    config = configparser.ConfigParser()
    config.read('config.ini')
    return config


def add_in_config(key, value):
    config = get_config()
    config['DEFAULT'][key] = value
    write_ini_file(config)

# basedir = os.path.abspath(os.path.dirname(__file__))
# db = SqliteDatabase(os.path.join(basedir, 'defect.sqlite'))