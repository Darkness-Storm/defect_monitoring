from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QMessageBox

from config import ROLE_ID
from gui.baselist import BaseModelList, BaseList
from gui.comoboxes import ComboManufacturer
from gui.delegate import ComponentComboBoxDelegate
from models import Purchase, PurchaseComp


class PurchaseList(BaseList):

    def __init__(self, parent=None):
        BaseList.__init__(self, parent)
        self.model = ModelPurchaseList(self)
        self.create_table()

    def initUI(self):

        main_layout = QtWidgets.QVBoxLayout()

        top_lay = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel("Период с ")
        self.date_start = QtWidgets.QDateEdit()
        self.date_start.setCalendarPopup(True)
        self.date_start.setCurrentSectionIndex(0)
        cur_date = QtCore.QDate().currentDate()
        self.date_start.setDate(
            QtCore.QDate(cur_date.year(), 1, 1))
        self.label_3 = QtWidgets.QLabel(" по ")
        self.date_end = QtWidgets.QDateEdit()
        self.date_end.setCalendarPopup(True)
        self.date_end.setDate(cur_date)
        spacer_top = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        top_lay.addWidget(self.label)
        top_lay.addWidget(self.date_start)
        top_lay.addWidget(self.label_3)
        top_lay.addWidget(self.date_end)
        top_lay.addSpacerItem(spacer_top)

        manufacturer_lay = QtWidgets.QHBoxLayout()
        self.label_2 = QtWidgets.QLabel("Изготовитель")
        self.comboBox = ComboManufacturer(parent=self)
        self.comboBox.setCurrentIndex(0)
        self.btn_filter = QtWidgets.QPushButton("ОК")

        spacer_man = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        manufacturer_lay.addWidget(self.label_2)
        manufacturer_lay.addWidget(self.comboBox)
        manufacturer_lay.addWidget(self.btn_filter)
        manufacturer_lay.addSpacerItem(spacer_man)

        # command panel lay
        cp_lay = QtWidgets.QHBoxLayout()
        self.btn_add = QtWidgets.QPushButton("Новая запись")

        self.btn_edit = QtWidgets.QPushButton('Редактировать запись')

        self.btn_del = QtWidgets.QPushButton("Удалить запись")

        spacer_cp = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        cp_lay.addWidget(self.btn_add)
        cp_lay.addWidget(self.btn_edit)
        cp_lay.addWidget(self.btn_del)
        cp_lay.addSpacerItem(spacer_cp)

        tv_lay = QtWidgets.QVBoxLayout()
        self.tv = QtWidgets.QTableView(self)

        tv_lay.addWidget(self.tv)

        bot_lay = QtWidgets.QHBoxLayout()
        self.btn_exit = QtWidgets.QPushButton("Выход", parent=self)

        spacer_bot = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        bot_lay.addSpacerItem(spacer_bot)
        bot_lay.addWidget(self.btn_exit)

        main_layout.addLayout(top_lay)
        main_layout.addLayout(manufacturer_lay)
        main_layout.addLayout(cp_lay)
        main_layout.addLayout(tv_lay)
        main_layout.addLayout(bot_lay)
        self.setLayout(main_layout)

    def init_signals(self):
        self.btn_filter.clicked.connect(self.create_table)
        self.btn_add.clicked.connect(self.new_rec)
        self.btn_edit.clicked.connect(self.edit_rec)
        self.btn_del.clicked.connect(self.del_rec)
        self.tv.doubleClicked.connect(self.edit_rec)
        self.btn_exit.clicked.connect(self.parent().closeActiveSubWindow)

    def new_rec(self, id_record=0):
        form = PurchaseDialog(parent=self, id_record=id_record)
        self.set_form_param(form)

    def set_form_param(self, form):
        form.resize(self.size().width(), self.size().height())
        form.setWindowModality(QtCore.Qt.WindowModal)
        form.show()

    def edit_rec(self):
        id = self.model.get_id(self.tv.currentIndex().row())
        self.new_rec(id)

    def set_param_tv(self):
        BaseList.set_param_tv(self)
        self.tv.hideColumn(self.model.get_fields()['id'])


class ModelPurchaseList(BaseModelList):

    model_db = Purchase

    def __init__(self, parent):
        BaseModelList.__init__(self, parent)

    def get_queryset(self):
        id = self.parent().comboBox.get_id()
        date_start = self.parent().date_start.date()
        date_end = self.parent().date_end.date()
        if id:
            queryset = self.get_model().select().where(
                (self.get_model().manufacturer == id)
                & ((self.get_model().datedoc >= date_start.toPyDate())
                & (self.get_model().datedoc <= date_end.toPyDate()))
            )
        else:
            queryset = self.get_model().select().where(
                (self.get_model().datedoc >= date_start.toPyDate()) & (
                        self.get_model().datedoc <= date_end.toPyDate())
            )
        return queryset

    def save(self):
        self.delete_recorddb()

    def delete_recorddb(self):
        for id in self._on_delete:
            record = self.get_model().get_by_id(int(id))
            for comp in record.components:
                comp.delete_instance()
            record.delete_instance()


class PurchaseDialog(QtWidgets.QDialog, BaseList):

    def __init__(self, parent, id_record):
        BaseList.__init__(self, parent)
        self.id_record = id_record
        self.set_data()
        self.model = ModelPurchaseDialog(self)
        self.create_table()

    def initUI(self):
        self.setWindowTitle('Test')
        main_lay = QtWidgets.QVBoxLayout()

        top_lay = QtWidgets.QHBoxLayout()
        self.lab_date = QtWidgets.QLabel('Дата документа', parent=self)
        self.date_doc = QtWidgets.QDateEdit(parent=self)
        self.date_doc.setCalendarPopup(True)
        self.date_doc.setCurrentSectionIndex(0)
        self.date_doc.setDate(QtCore.QDate.currentDate())
        self.lab_man = QtWidgets.QLabel("Изготовитель", parent=self)
        self.combo_man = ComboManufacturer(parent=self)
        spacer_top = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        top_lay.addWidget(self.lab_date)
        top_lay.addWidget(self.date_doc)
        top_lay.addWidget(self.lab_man)
        top_lay.addWidget(self.combo_man)
        top_lay.addSpacerItem(spacer_top)

        cp_lay = QtWidgets.QHBoxLayout()
        self.btn_new_rec = QtWidgets.QPushButton('Новая запись', parent=self)
        self.btn_edit = QtWidgets.QPushButton('Изменить запись', parent=self)
        self.btn_del = QtWidgets.QPushButton('Удалить запись', parent=self)
        spacer_cp = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        cp_lay.addWidget(self.btn_new_rec)
        cp_lay.addWidget(self.btn_edit)
        cp_lay.addWidget(self.btn_del)
        cp_lay.addSpacerItem(spacer_cp)

        tbl_lay = QtWidgets.QHBoxLayout()
        self.tv = QtWidgets.QTableView(self)
        tbl_lay.addWidget(self.tv)

        bot_lay = QtWidgets.QHBoxLayout()
        self.btn_close = QtWidgets.QPushButton('Выход', parent=self)

        spacer_bot = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        bot_lay.addSpacerItem(spacer_bot)
        bot_lay.addWidget(self.btn_close)

        main_lay.addLayout(top_lay)
        main_lay.addLayout(cp_lay)
        main_lay.addLayout(tbl_lay)
        main_lay.addLayout(bot_lay)
        self.setLayout(main_lay)

    def init_signals(self):
        self.btn_new_rec.clicked.connect(self.new_rec)
        self.btn_edit.clicked.connect(self.edit_rec)
        self.btn_del.clicked.connect(self.del_rec)
        self.btn_close.clicked.connect(self.close)

    def edit_rec(self):
        pass
        # print(self.model.on_update)

    def set_param_tv(self):
        BaseList.set_param_tv(self)
        self.tv.hideColumn(self.model.get_fields()['id'])
        self.tv.hideColumn(self.model.get_fields()['purchase'])
        self.tv.setItemDelegateForColumn(self.model.get_fields()['component'],
                                         ComponentComboBoxDelegate(self))

    def set_data(self):
        if self.id_record:
            self.data = Purchase.get_or_none(id=self.id_record)
            self.date_doc.setDate(self.data.datedoc)
            self.combo_man.setCurrentIndex(
                self.combo_man.findData(self.data.manufacturer.id))
        else:
            self.data = Purchase()

    def save(self):
        self.data.datedoc = self.date_doc.date().toPyDate()
        self.data.manufacturer = self.combo_man.get_id()
        self.data.save()
        if not self.id_record:
            self.id_record = self.data.id
        self.model.fill_parent_id(self.id_record)
        self.model.save()

    def closeEvent(self, event):
        # if self.model.on_delete or self.model.on_update:
        user_select = QMessageBox.question(
            self,
            'Выход',
            "Хотите сохранить запись? " +
            "(Save-сохранить, Discard-без сохранения, cancel-не закрывать)",
            QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel,
            QMessageBox.Cancel
        )
        if user_select == QMessageBox.Save:
            self.save()
            event.accept()
        if user_select == QMessageBox.Discard:
            event.accept()
        if user_select == QMessageBox.Cancel:
            event.ignore()
        # else:
        #     event.accept()


class ModelPurchaseDialog(BaseModelList):

    model_db = PurchaseComp

    def __init__(self, parent):
        BaseModelList.__init__(self, parent)

    def get_queryset(self):
        id = self.parent().id_record
        return self.get_model().select().where(
            (self.get_model().purchase == id)
        )

    def fill_parent_id(self, id):
        for index_row in range(self.rowCount()):
            parent_id = self.get_item_data(index_row, 'purchase')
            if not parent_id:
                self.item(index_row, self.get_fields()['purchase']).setData(id, ROLE_ID)
