from gui.baselist import BaseModelList, BaseList
from gui.delegate import GroupComboBoxDelegate
from models import *


class ModelManufacturerList(BaseModelList):

    model_db = Manufacturer

    def __init__(self, parent):
        BaseModelList.__init__(self, parent)

    def get_queryset(self):
        return self.get_model().select().order_by(+self.get_model().name)


class ManufacturerList(BaseList):

    def __init__(self, parent):
        BaseList.__init__(self, parent)
        self.model = ModelManufacturerList(self)
        self.create_table()


class ModelGroupList(BaseModelList):

    model_db = GroupDefect

    def __init__(self, parent):
        BaseModelList.__init__(self, parent)

    def get_queryset(self):
        return self.get_model().select().order_by(+self.get_model().name)


class GroupList(BaseList):

    def __init__(self, parent):
        BaseList.__init__(self, parent)
        self.model = ModelGroupList(self)
        self.create_table()


class ModelDefectList(BaseModelList):

    model_db = Defect

    def __init__(self, parent):
        BaseModelList.__init__(self, parent)

    def get_queryset(self):
        return self.get_model().select().order_by(+self.get_model().name)


class DefectList(BaseList):

    def __init__(self, parent):
        BaseList.__init__(self, parent)
        self.model = ModelDefectList(self)
        self.create_table()

    def set_param_tv(self):
        BaseList.set_param_tv(self)
        # self.tv.setItemDelegateForColumn(self.model.fields['group'], GroupComboBoxDelegate(self))
        self.tv.setItemDelegateForColumn(2, GroupComboBoxDelegate(self))


class ModelComponentList(BaseModelList):

    model_db = Component

    def __init__(self, parent):
        BaseModelList.__init__(self, parent)

    def get_queryset(self):
        return self.get_model().select().order_by(+self.get_model().name)


class ComponentList(BaseList):

    def __init__(self, parent):
        BaseList.__init__(self, parent)
        self.model = ModelComponentList(self)
        self.create_table()


class ModelPlaceList(BaseModelList):

    model_db = PlaceDetection

    def __init__(self, parent):
        BaseModelList.__init__(self, parent)


class PlaceList(BaseList):

    def __init__(self, parent):
        BaseList.__init__(self, parent)
        self.model = ModelPlaceList(self)
        self.create_table()
