import datetime
import os
import tempfile
import platform

from PyQt5.QtWidgets import QTableWidget, QWidget, QHBoxLayout, QLabel, QDateEdit, QSpacerItem, QSizePolicy, \
    QPushButton, QVBoxLayout, QListView, QAbstractItemView, QMessageBox
from PyQt5 import QtCore, QtGui
import xlsxwriter

from gui.comoboxes import ComboManufacturer
from models import *
from config import basedir


class BaseTable(QTableWidget):

    def __init__(self, *args, **kwargs):
        super(BaseTable, self).__init__(*args, *kwargs)
        self.setColumnCount(100)
        self.setColumnWidth(0, 5)
        self.resizeColumnsToContents()
        self.setRowCount(100)


class ListReport(QListView):

    reports = ['Ежемесячные сведения', 'Нарастающим итогом', 'Прочие']

    def __init__(self, *args, **kwargs):
        super(ListReport, self).__init__(*args, **kwargs)
        self.add_items()
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)

    def add_items(self):
        model = QtGui.QStandardItemModel(self)
        for report in self.reports:
            item = QtGui.QStandardItem(report)
            model.appendRow(item)
        self.setModel(model)

    def get_name_report(self):
        row = self.currentIndex().row()
        if row == -1:
            row = 0
        return self.model().item(row).text()


class BaseWidget(QWidget):

    def __init__(self, *args, **kwargs):
        super(BaseWidget, self).__init__(*args, **kwargs)
        self.initUI()
        self.init_signals()

    def initUI(self):
        main_layout = QVBoxLayout()

        top_lay = QHBoxLayout()
        self.label = QLabel("Период с ")
        self.date_start = QDateEdit()
        self.date_start.setCalendarPopup(True)
        self.date_start.setCurrentSectionIndex(0)
        cur_date = QtCore.QDate().currentDate()
        self.date_start.setDate(
            QtCore.QDate(cur_date.year(), 1, 1))
        self.label_3 = QLabel(" по ")
        self.date_end = QDateEdit()
        self.date_end.setCalendarPopup(True)
        self.date_end.setDate(cur_date)
        spacer_top = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        top_lay.addWidget(self.label)
        top_lay.addWidget(self.date_start)
        top_lay.addWidget(self.label_3)
        top_lay.addWidget(self.date_end)
        top_lay.addSpacerItem(spacer_top)

        manufacturer_lay = QHBoxLayout()
        self.label_2 = QLabel("Изготовитель")
        self.comboBox = ComboManufacturer(parent=self)
        self.comboBox.setCurrentIndex(0)
        spacer_man = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        manufacturer_lay.addWidget(self.label_2)
        manufacturer_lay.addWidget(self.comboBox)
        manufacturer_lay.addSpacerItem(spacer_man)

        list_lay = QVBoxLayout()
        self.list_rep = ListReport(self)
        spacer_main = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        list_lay.addWidget(self.list_rep)
        list_lay.addSpacerItem(spacer_main)


        bot_lay = QHBoxLayout()
        self.btn_ok = QPushButton('Сформировать', parent=self)
        self.btn_exit = QPushButton("Выход", parent=self)
        spacer_bot = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        bot_lay.addSpacerItem(spacer_bot)
        bot_lay.addWidget(self.btn_ok)
        bot_lay.addWidget(self.btn_exit)

        bot_lay_sp = QVBoxLayout()
        spacer_main = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        bot_lay_sp.addSpacerItem(spacer_main)

        main_layout.addLayout(top_lay)
        main_layout.addLayout(manufacturer_lay)
        main_layout.addLayout(list_lay)
        main_layout.addLayout(bot_lay)
        main_layout.addLayout(bot_lay_sp)
        self.setLayout(main_layout)

    def init_signals(self):
        self.btn_exit.clicked.connect(self.parent().closeActiveSubWindow)
        self.btn_ok.clicked.connect(self.run)
        self.list_rep.doubleClicked.connect(self.run)

    def run(self):
        if self.check_filling():
            name_report = self.list_rep.get_name_report()

            if name_report == 'Ежемесячные сведения':
                self.monthly_info()
            elif name_report == 'Нарастающим итогом':
                self.cumulative_info()

    def check_filling(self):
        msg = QMessageBox(QMessageBox.Critical, 'Ошибка заполнения', '', buttons=QMessageBox.Ok, parent=self)
        if not self.date_start:
            msg.setText('Не введена начальная дата!')
            msg.exec()
            return False
        if not self.date_end:
            msg.setText('Не введена конечная дата!')
            msg.exec()
            return False
        if not self.comboBox.currentIndex():
            msg.setText('Не выбран изготовитель!')
            msg.exec()
            return False
        return True

    def _get_period_for_header(self, start, end):
        if start.month == end.month:
            return 'за ' + datetime.date.strftime(start, '%B %Y г.')
        else:
            return 'за период с ' + datetime.date.strftime(start, '%x') + ' по ' + datetime.date.strftime(end, '%x')

    def _getformat(self, name_format):
        format = self.workbook.add_format()
        if name_format == 'header' or name_format == 'table_header':
            format.set_bold(True)
            format.set_border(0)
            format.set_align('center')
            format.set_valign('vcenter')
            if name_format == 'table_header':
                format.set_border(1)
        elif name_format == 'number' or name_format == 'percent' \
                or name_format == 'number_group' or name_format == 'percent_group':
            format.set_border(1)
            format.set_align('center')
            format.set_valign('vcenter')
            if name_format == 'percent':
                format.set_num_format('0.0%')
            if name_format == 'number_group':
                format.set_bold(True)
            if name_format == 'percent_group':
                format.set_bold(True)
                format.set_num_format('0.0%')
        elif name_format == 'l_border':
            format.set_left(1)
        elif name_format == 'r_border':
            format.set_right(1)
        elif name_format == 'name_group':
            format.set_bold(True)
            format.set_border(1)
            format.set_valign('vcenter')
        elif name_format == 'defect':
            format.set_border(1)
            format.set_text_wrap(True)
        elif name_format == 'bold':
            format.set_bold(True)
        elif name_format == 'center':
            format.set_align('center')
            format.set_valign('vcenter')

        return format
    
    @staticmethod
    def _get_file():
        dir = os.path.join(basedir, 'tmp')
        if not os.path.exists(dir):
            try:
                os.mkdir(dir)
            except OSError:
                print('Не удалось создать папку ' + dir)
                dir = tempfile.gettempdir()
        date_str = datetime.datetime.strftime(datetime.datetime.now(), '%Y_%m_%d_%H_%M_%S_%f')
        return os.path.join(dir, 'report_' + date_str + '.xlsx')

    def monthly_info(self):

        file = self._get_file()
        workbook = xlsxwriter.Workbook(file)
        worksheet = workbook.add_worksheet()
        worksheet.set_column(0, 0, 4)
        worksheet.set_column(1, 1, 70)

        # заголовок формы
        manufacturer_id = self.comboBox.get_id()
        try:
            manufacturer_str = Manufacturer.get_by_id(int(manufacturer_id)).full_name()
        except (DoesNotExist, AttributeError):
            manufacturer_str = ''
        start = self.date_start.date().toPyDate()
        end = self.date_end.date().toPyDate()
        header = []
        period = self._get_period_for_header(start, end)
        header.append('Сведения')
        header.append('по дефектам двигателей  производства ' + manufacturer_str)
        header.append('выявленным в процессе производства ' + period)
        header_format = workbook.add_format({
            'bold': True,
            'border': 0,
            'align': 'center',
            'valign': 'vcenter'
        })
        x = 0
        for idx, value in enumerate(header):
            worksheet.merge_range(idx, 0, idx, 1, value, header_format)
            x += idx
        # строки компонентов
        component_format = workbook.add_format({'bold': True})
        components = DefectiveComponent\
            .select(DefectiveComponent.component)\
            .group_by(DefectiveComponent.component)\
            .join(Monitoring)\
            .where((Monitoring.datedoc.between(start, end))&(Monitoring.manufacturer_id==manufacturer_id))

        for component in components:
            x += 1
            worksheet.write(x, 0, component.component.full_name(), component_format)
            x += 2
            # дефекты
            defects = DefectiveComponent\
                .select()\
                .where(DefectiveComponent.component == component.component)\
                .join(Monitoring)\
                .where(Monitoring.datedoc.between(start, end))

            for defect in defects:
                defect_format = workbook.add_format()
                defect_format.set_text_wrap()
                worksheet.write(x, 1, defect.serial + ' - ' + defect.defect.name, defect_format)
                x += 1

        workbook.close()
        self.run_ex_app(file)

    def cumulative_info(self):
        file = self._get_file()
        self.workbook = xlsxwriter.Workbook(file)
        worksheet = self.workbook.add_worksheet()

        worksheet.set_column(0, 0, 1)
        worksheet.set_column(1, 1, 5)
        worksheet.set_column(2, 2, 60)
        worksheet.set_column(3, 4, 10)

        # заголовок формы
        manufacturer_id = self.comboBox.get_id()
        try:
            manufacturer_str = Manufacturer.get_by_id(int(manufacturer_id)).full_name()
        except (DoesNotExist, AttributeError):
            manufacturer_str = ''
        start = self.date_start.date().toPyDate()
        end = self.date_end.date().toPyDate()
        header = []
        period = self._get_period_for_header(start, end)
        header.append('Сведения')
        header.append('по дефектам двигателей  производства ' + manufacturer_str)
        header.append('выявленным в процессе производства ' + period)

        x = 0 # указатель позиции строки
        for idx, value in enumerate(header):
            worksheet.merge_range(idx, 0, idx, 4, value, self._getformat('header'))
            x += idx

        query = DefectiveComponent\
            .select()\
            .join(Monitoring)\
            .where((Monitoring.datedoc.between(start, end))&(Monitoring.manufacturer_id==manufacturer_id))
        count_defect = query.count()
        count_purchase = PurchaseComp\
            .select(fn.Sum(PurchaseComp.number))\
            .join(Purchase)\
            .where((Purchase.datedoc.between(start, end))&(Purchase.manufacturer_id == manufacturer_id))\
            .scalar()
        x += 2
        number = 1 # порядковые номера дефектов

        worksheet.write(x, 1, '№', self._getformat('table_header'))
        worksheet.write(x, 2, 'Наименование дефектов', self._getformat('table_header'))
        worksheet.write(x, 3, 'Количество', self._getformat('table_header'))
        worksheet.write(x, 4, 'Процент', self._getformat('table_header'))

        groups = query\
            .select(DefectiveComponent.defect, fn.Count(Defect.group).alias('count_group'))\
            .join(Defect, on=(DefectiveComponent.defect_id==Defect.id))\
            .group_by(DefectiveComponent.defect.group)

        for group in groups:
            x += 1
            worksheet.write(x, 1, '', self._getformat('l_border'))
            worksheet.write(x, 2, group.defect.group.name, self._getformat('bold'))
            worksheet.write(x, 4, '', self._getformat('r_border'))
            x += 1
            defects = query\
                .select(DefectiveComponent.defect, fn.Count(Defect.id).alias('count_defect'))\
                .join(Defect, on=(DefectiveComponent.defect_id==Defect.id))\
                .where(DefectiveComponent.defect.group == group.defect.group)\
                .group_by(DefectiveComponent.defect)

            for defect in defects:
                worksheet.write(x, 1, number, self._getformat('number'))
                worksheet.write(x, 2, defect.defect.name, self._getformat('defect'))
                worksheet.write(x, 3, defect.count_defect, self._getformat('number'))
                worksheet.write(x, 4, defect.count_defect/count_defect, self._getformat('percent'))
                number += 1
                x += 1

            worksheet.write(x, 1, '', self._getformat('l_border'))
            worksheet.write(x, 4, '', self._getformat('r_border'))
            x += 1
            worksheet.write(x, 1, '', self._getformat('name_group'))
            worksheet.write(x, 2, 'Итого по группе: ' + group.defect.group.name, self._getformat('name_group'))
            worksheet.write(x, 3, group.count_group, self._getformat('number_group'))
            worksheet.write(x, 4, group.count_group/count_defect, self._getformat('percent_group'))
            x += 1
            worksheet.write(x, 1, '', self._getformat('l_border'))
            worksheet.write(x, 4, '', self._getformat('r_border'))

        x += 1
        worksheet.write(x, 1, '', self._getformat('name_group'))
        worksheet.write(x, 2, 'Итого', self._getformat('name_group'))
        worksheet.write(x, 3, count_defect, self._getformat('number_group'))
        try:
            percent = count_defect/count_purchase
        except (TypeError, ZeroDivisionError):
            percent = 0

        worksheet.write(x, 4, percent, self._getformat('percent_group'))

        # Подвал отчета
        param = PlaceDetection.get(PlaceDetection.name == 'Входной контроль')
        incoming = query.select().where(DefectiveComponent.place == param)
        incoming_fix = incoming.select().where(DefectiveComponent.returns==False).count()
        incoming_returns = incoming.select().where(DefectiveComponent.returns==True).count()

        other = query.select().where(DefectiveComponent.place != param)
        other_fix = other.select().where(DefectiveComponent.returns==False).count()
        other_returns = other.select().where(DefectiveComponent.returns==True).count()
        center = self._getformat('center')
        x += 2
        worksheet.write(x, 2, 'Примечание')
        x += 1
        worksheet.write(x, 2, 'Выдано в производство, шт.')
        worksheet.write(x, 3, count_purchase, center)
        x += 1
        worksheet.write(x, 2, 'Доработанных и забракованных в ООО ПАЗ, шт.')
        worksheet.write(x, 3, count_defect, center)
        x += 1
        worksheet.write(x, 2, 'в т.ч. выявленных при входном контроле, шт.')
        worksheet.write(x, 3, incoming.count(), center)
        x += 1
        worksheet.write(x, 2, ' '*4 + 'из них: доработанных на складе, шт.')
        worksheet.write(x, 3, incoming_fix, center)
        x += 1
        worksheet.write(x, 2, ' '*4 + 'признанных окончательным браком, шт.')
        worksheet.write(x, 3, incoming_returns, center)
        x += 1
        worksheet.write(x, 2, 'в т.ч. выявленных в производстве, шт.')
        worksheet.write(x, 3, other.count(), center)
        x += 1
        worksheet.write(x, 2, ' '*4 + 'из них: доработанных представителями, шт.')
        worksheet.write(x, 3, other_fix, center)
        x += 1
        worksheet.write(x, 2, ' '*4 + 'признанных окончательным браком, шт.')
        worksheet.write(x, 3, other_returns, center)

        self.workbook.close() # повтор
        self.run_ex_app(file) # повтор

    def run_ex_app(self, file):
        if platform.system() == 'Linux':
            #try:
            os.system('libreoffice ' + file)
            #except Exception as e:
            #    print(e)
        #subprocess.call('libreoffice ' + file.name)
        elif platform.system() == 'Windows':
            os.startfile(file)
