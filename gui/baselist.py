from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtWidgets import QMessageBox

from config import ROLE_ID


class BaseModelList(QtGui.QStandardItemModel):

    """Создает экземпляр модели QtGui.QStandardItemModel
    с добавлением методов необходимых для работы с моделями peewee.
    Для использования необходимо создать класс, который наследуется от базового
    и обозначить используемую модель peewee
    """

    model_db = None

    def __init__(self, parent=None):
        QtGui.QStandardItemModel.__init__(self, parent)
        self._on_delete = set()
        self._on_update = set()
        self._fields = {}

    def create(self):
        self.clear()
        self.setColumnCount(len(self.get_fields()))
        self.set_headers()
        self.set_data()

    def get_fields(self):
        if not self._fields:
            self._fields = {str(field): index for index, field in enumerate(self.get_model()._meta.fields.keys())}
                # self.fields = list(self.get_model()._meta.fields.keys())
        return self._fields

    def _get_field_type(self, field):
        return self.get_model()._meta.fields[field].__class__.__name__

    def _get_verbose_name(self, field):
        if field == 'id':
            return 'Код'
        else:
            return self.get_model()._meta.fields[field].verbose_name

    def set_headers(self):
        for key, value in self.get_fields().items():
            self.setHeaderData(value, QtCore.Qt.Horizontal, self._get_verbose_name(key))

    def get_model(self):
        return self.model_db

    def get_queryset(self):
        return self.get_model().select()

    def set_item_pref(self, item, field):
        field_type = self._get_field_type(field)
        if field_type == 'BooleanField':
            item.setCheckable(True)
            item.setTristate(False)
        elif field_type == 'AutoField':
            item.setEditable(False)
        return item

    def set_item_data(self, item, field, row):
        field_type = self._get_field_type(field)
        if field_type == 'BooleanField':
            item.setCheckState(eval('row.' + field) * 2)
        elif field_type == 'ForeignKeyField':
            item.setText(eval('row.' + field + '.__str__()'))
            item.setData(eval('row.' + field), ROLE_ID)
        else:
            item.setText(eval('row.' + field + '.__str__()'))
        return item

    def get_item_data(self, index_row, field):
        field_type = self._get_field_type(field)
        if field_type == 'BooleanField':
            return self.item(index_row, self.get_fields()[field]).checkState() / 2
        elif field_type == 'ForeignKeyField':
            return self.item(index_row, self.get_fields()[field]).data(ROLE_ID)
        else:
            return self.item(index_row, self.get_fields()[field]).text()

    def add_row(self):
        self.appendRow([self.set_item_pref(QtGui.QStandardItem(), field) for field in self.get_fields()])

    def set_row(self, row):
        self.appendRow([self.set_item_data(
            self.set_item_pref(QtGui.QStandardItem(), field), field, row) for field in self.get_fields()])

    def set_data(self):
        for row in self.get_queryset():
            self.set_row(row)

    def get_id(self, row):
        return self.item(row, self.get_fields()['id']).text()

    def row_to_dict(self, row):
        return {field: self.get_item_data(row, field) for field in self.get_fields().keys()}

    def set_on_update(self, row):
        self._on_update.add(self.get_id(row))

    def set_on_delete_rows(self, rows):
        for row in rows:
            self._on_delete.add(self.get_id(row.row()))
        for id in self._on_delete:
            result = self.findItems(id, QtCore.Qt.MatchContains, self.get_fields()['id'])
            if result:
                row = result[0]
                self.removeRow(row.row())

    def _delete_record_db(self):
        try:
            self.get_model().delete().where(self.get_model().id.in_(self._on_delete)).execute()
        except Exception as e:
            print(e)

    def _update_record_db(self):
        for id in self._on_update:
            result = self.findItems(id, QtCore.Qt.MatchContains, self.get_fields()['id'])
            if result:
                row = result[0]
                self.get_model().replace(**self.row_to_dict(row.row())).execute()
        for index_row in range(self.rowCount()):
            if not self.get_id(index_row):
                row_dict = self.row_to_dict(index_row)
                # if dict contains 'id' == '', method 'create' fails
                row_dict.pop('id')
                self.get_model().create(**row_dict)

    def save(self):
        self._delete_record_db()
        self._update_record_db()

    def is_change(self):
        if self._on_delete or self._on_update:
            return True
        else:
            return False
    
    def is_valiid(self):
        for field in self.get_fields():
            field_type = self._get_field_type(field)
            if field_type == 'ForeignKeyField':
                pass


class BaseList(QtWidgets.QWidget):

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.initUI()
        self.init_signals()

    def initUI(self):
        self.main_lay = QtWidgets.QVBoxLayout(self)
        self.cp_lay = QtWidgets.QHBoxLayout()
        self.btn_add = QtWidgets.QPushButton('Новая запись', parent=self)

        # self.btn_edit = QtWidgets.QPushButton('Изменить запись', parent=self)
        # self.btn_edit.clicked.connect(self.edit_rec)
        self.btn_del = QtWidgets.QPushButton('Удалить запись', parent=self)

        self.spacer_cp = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.cp_lay.addWidget(self.btn_add)
        # cp_lay.addWidget(self.btn_edit)
        self.cp_lay.addWidget(self.btn_del)
        self.cp_lay.addSpacerItem(self.spacer_cp)

        self.tbl_lay = QtWidgets.QHBoxLayout()
        self.tv = QtWidgets.QTableView(self)

        self.tbl_lay.addWidget(self.tv)

        self.bot_lay = QtWidgets.QHBoxLayout()
        self.btn_close = QtWidgets.QPushButton('Выход', parent=self)

        spacer_bot = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.bot_lay.addSpacerItem(spacer_bot)
        self.bot_lay.addWidget(self.btn_close)

        self.main_lay.addLayout(self.cp_lay)
        self.main_lay.addLayout(self.tbl_lay)
        self.main_lay.addLayout(self.bot_lay)
        self.setLayout(self.main_lay)

    def init_signals(self):
        self.btn_add.clicked.connect(self.new_rec)
        self.btn_del.clicked.connect(self.del_rec)
        self.btn_close.clicked.connect(self.parent().closeActiveSubWindow)
        self.tv.doubleClicked.connect(self.edit_rec)

    def new_rec(self):
        last_index = self.model.rowCount()
        self.model.add_row()
        self.tv.selectRow(last_index)

    def edit_rec(self):
        row = self.tv.currentIndex().row()
        self.model.set_on_update(row)

    def del_rec(self):
        rows = self.tv.selectionModel().selectedRows()
        self.model.set_on_delete_rows(rows)

    def create_table(self):
        self.model.create()
        self.set_param_tv()

    def set_param_tv(self):
        self.tv.setModel(self.model)
        self.tv.setSelectionBehavior(1)
        self.tv.verticalHeader().hide()
        self.tv.resizeColumnsToContents()
        self.tv.resizeRowsToContents()

    def closeEvent(self, event):
        if self.model.is_change():
            user_select = QMessageBox.question(
                self,
                'Выход',
                "Хотите сохранить запись? " +
                "(Save-сохранить, Discard-без сохранения, cancel-не закрывать)",
                QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel,
                QMessageBox.Cancel
            )
            if user_select == QMessageBox.Save:
                self.model.save()
                event.accept()
            if user_select == QMessageBox.Discard:
                event.accept()
            if user_select == QMessageBox.Cancel:
                event.ignore()
        else:
            event.accept()
