import os
from functools import partial

from PyQt5 import QtCore, QtWidgets

from config import basedir
from gui.monitoring import MonitoringList
from gui.list import ManufacturerList, GroupList, DefectList, ComponentList, PlaceList
from gui.purchase import PurchaseList
from gui.report import BaseTable, BaseWidget
from models import exists_db


class MyWindow(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        exists_db()
        self.n = 1
        self.mdi_area = QtWidgets.QMdiArea()
        # self.mdi_area.resize(960, 850)
        self.setCentralWidget(self.mdi_area)
        self.add_menu()
        self.init_signals()
        self.setWindowTitle("Мониторинг дефектов")

    def add_menu(self):

        self.act_mon = QtWidgets.QAction('Журнал мониторинга', self)
        self.act_pur = QtWidgets.QAction('Закупки', self)
        self.menu_reg = QtWidgets.QMenu('Журналы регистрации')
        self.menu_reg.addAction(self.act_mon)
        self.menu_reg.addAction(self.act_pur)

        self.list_defects = QtWidgets.QAction('Дефекты', self)
        self.list_group = QtWidgets.QAction('Группы дефектов', self)
        self.list_manufacturer = QtWidgets.QAction('Изготовители', self)
        self.list_component = QtWidgets.QAction('Компоненты', self)
        self.list_place = QtWidgets.QAction('Места обнаружения', self)
        self.menu_refer = QtWidgets.QMenu('Справочники', self)
        self.menu_refer.addAction(self.list_defects)
        self.menu_refer.addAction(self.list_group)
        self.menu_refer.addAction(self.list_manufacturer)
        self.menu_refer.addAction(self.list_component)
        self.menu_refer.addAction(self.list_place)

        self.reports = QtWidgets.QMenu('Отчеты')
        self.report_engine = QtWidgets.QAction('Отчеты по двигателям', self)
        self.reports.addAction(self.report_engine)

        self.menuWindow = QtWidgets.QMenu("&Окно")
        self.actCascade = QtWidgets.QAction("Каскадом", self)
        self.menuWindow.addAction(self.actCascade)
        self.actTile = QtWidgets.QAction("Равномерно", self)
        self.menuWindow.addAction(self.actTile)
        self.actCloseActive = QtWidgets.QAction("Закрыть активное", self)
        self.menuWindow.addAction(self.actCloseActive)
        self.actCloseAll = QtWidgets.QAction("Закрыть все", self)
        self.menuWindow.addAction(self.actCloseAll)
        self.exit = QtWidgets.QAction('Выход', self)

        self.menuBar().addMenu(self.menu_reg)
        self.menuBar().addMenu(self.menu_refer)
        self.menuBar().addMenu(self.reports)
        self.menuBar().addMenu(self.menuWindow)
        self.menuBar().addAction(self.exit)

    def init_signals(self):
        self.act_mon.triggered.connect(partial(self.open_form, MonitoringList))
        self.act_pur.triggered.connect(partial(self.open_form, PurchaseList))
        self.list_defects.triggered.connect(partial(self.open_form, DefectList))
        self.list_group.triggered.connect(partial(self.open_form, GroupList))
        self.list_component.triggered.connect(partial(self.open_form, ComponentList))
        self.list_manufacturer.triggered.connect(partial(self.open_form, ManufacturerList))
        self.list_place.triggered.connect(partial(self.open_form, PlaceList))
        self.report_engine.triggered.connect(partial(self.open_form, BaseWidget))
        self.actCascade.triggered.connect(self.mdi_area.cascadeSubWindows)
        self.actTile.triggered.connect(self.mdi_area.tileSubWindows)
        self.actCloseActive.triggered.connect(self.mdi_area.closeActiveSubWindow)
        self.actCloseAll.triggered.connect(self.mdi_area.closeAllSubWindows)
        self.exit.triggered.connect(self.close)

    def open_form(self, form):
        if form:
            w = form(self.mdi_area)
            sWindow = self.mdi_area.addSubWindow(w)
            sWindow.setAttribute(QtCore.Qt.WA_DeleteOnClose)
            sWindow.showMaximized()
        else:
            print('None')

    def closeEvent(self, event):
        tmpdir = os.path.join(basedir, 'tmp')
        list_file = os.listdir(tmpdir)
        for f in list_file:
            file_name = os.path.join(tmpdir, f)
            try:
                os.remove(file_name)
            except TypeError as e:
                print('не удалось удалить файл ' + f)
                print(e)
        event.accept()
