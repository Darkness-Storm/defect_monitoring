from PyQt5 import QtWidgets, QtCore

from gui.comoboxes import ComboComponent, ComboDefect, ComboPlace, ComboGroup
from config import ROLE_ID


class ComponentComboBoxDelegate(QtWidgets.QItemDelegate):

    def __init__(self, parent):
        super(ComponentComboBoxDelegate, self).__init__(parent)

    def createEditor(self, parent, option, index):
        return ComboComponent(parent)

    def setEditorData(self, editor, index):
        editor.setCurrentIndex(editor.currentIndex())

    def setModelData(self, editor, model, index):
        model.setData(index, editor.currentText())
        model.setData(index, editor.currentData(), ROLE_ID)


class DefectComboBoxDelegate(QtWidgets.QItemDelegate):

    def __init__(self, parent):
        super(DefectComboBoxDelegate, self).__init__(parent)

    def createEditor(self, parent, option, index):
        return ComboDefect(parent)

    def setEditorData(self, editor, index):
        editor.setCurrentIndex(editor.currentIndex())

    def setModelData(self, editor, model, index):
        model.setData(index, editor.currentText())
        model.setData(index, editor.currentData(), ROLE_ID)


class PlaceComboBoxDelegate(QtWidgets.QItemDelegate):

    def __init__(self, parent):
        super(PlaceComboBoxDelegate, self).__init__(parent)

    def createEditor(self, parent, option, index):
        return ComboPlace(parent)

    def setEditorData(self, editor, index):
        editor.setCurrentIndex(editor.currentIndex())

    def setModelData(self, editor, model, index):
        model.setData(index, editor.currentText())
        model.setData(index, editor.currentData(), ROLE_ID)


class CheckBoxDelegate(QtWidgets.QItemDelegate):

    def __init__(self, parent):
        super(CheckBoxDelegate, self).__init__(parent)


class GroupComboBoxDelegate(QtWidgets.QItemDelegate):

    def __init__(self, parent):
        super(GroupComboBoxDelegate, self).__init__(parent)

    def createEditor(self, parent, option, index):
        return ComboGroup(parent)

    def setEditorData(self, editor, index):
        editor.setCurrentIndex(editor.currentIndex())

    def setModelData(self, editor, model, index):
        model.setData(index, editor.currentText())
        model.setData(index, editor.currentData(), ROLE_ID)