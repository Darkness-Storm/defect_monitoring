from config import ROLE_ID
from gui.baselist import BaseModelList, BaseList
from gui.delegate import ComponentComboBoxDelegate, DefectComboBoxDelegate, PlaceComboBoxDelegate
from gui.purchase import ModelPurchaseList, PurchaseList, PurchaseDialog
from models import Monitoring, DefectiveComponent


class ModelMonitoringList(ModelPurchaseList):

    model_db = Monitoring

    def __init__(self, parent=None):
        ModelPurchaseList.__init__(self, parent)


class MonitoringList(PurchaseList):

    def __init__(self, parent=None):
        BaseList.__init__(self, parent)
        self.model = ModelMonitoringList(self)
        self.create_table()

    def new_rec(self, id_record=0):
        form = MonitoringDialog(parent=self, id_record=id_record)
        self.set_form_param(form)


class ModelMonitoringDialog(BaseModelList):

    model_db = DefectiveComponent

    def __init__(self, parent):
        BaseModelList.__init__(self, parent)

    def get_queryset(self):
        id = self.parent().id_record
        return self.get_model().select().where(
            (self.get_model().monitoring == id)
        )

    def fill_parent_id(self, id):
        for index_row in range(self.rowCount()):
            parent_id = self.get_item_data(index_row, 'monitoring')
            if not parent_id:
                self.item(index_row, self.get_fields()['monitoring']).setData(id, ROLE_ID)


class MonitoringDialog(PurchaseDialog):

    def __init__(self, parent, id_record):
        BaseList.__init__(self, parent)
        self.id_record = id_record
        self.set_data()
        self.model = ModelMonitoringDialog(self)
        self.create_table()
        print(self.parent().size().width())
        self.resize(self.parent().size())

    def set_param_tv(self):
        BaseList.set_param_tv(self)
        self.tv.hideColumn(self.model.get_fields()['id'])
        self.tv.hideColumn(self.model.get_fields()['monitoring'])
        self.tv.setItemDelegateForColumn(self.model.get_fields()['component'],
                                         ComponentComboBoxDelegate(self))
        self.tv.setItemDelegateForColumn(self.model.get_fields()['defect'], DefectComboBoxDelegate(self))
        self.tv.setItemDelegateForColumn(self.model.get_fields()['place'], PlaceComboBoxDelegate(self))
        self.tv.setColumnWidth(self.model.get_fields()['component'], 250)
        self.tv.setColumnWidth(self.model.get_fields()['defect'], 650)
        # self.tv.setColumnWidth(self.model.fields['defect'], 250)
        # self.resize(self.parent().size())

    def set_data(self):
        if self.id_record:
            self.data = Monitoring.get(Monitoring.id == self.id_record)
            self.date_doc.setDate(self.data.datedoc)
            self.combo_man.setCurrentIndex(
                self.combo_man.findData(self.data.manufacturer.id))
        else:
            self.data = Monitoring()
