from PyQt5 import QtWidgets

from models import Manufacturer, Component, Defect, PlaceDetection, GroupDefect


class ComboBoxBase(QtWidgets.QComboBox):

    model = None

    def __init__(self, parent=None):
        QtWidgets.QComboBox.__init__(self, parent)
        self.add_items()

    def get_id(self):
        return self.itemData(self.currentIndex())

    def get_model(self):
        return self.model

    def get_queryset(self):
        return self.get_model().select().order_by(+self.get_model().name)

    def add_items(self):
        self.addItem('', 0)
        for item in self.get_queryset():
            self.addItem(item.__str__(), item.id)

    def get_list_text(self):
        lst = []
        for index in range(self.count()):
            text = self.itemText(index)
            lst.append(text)
        return lst


class ComboManufacturer(ComboBoxBase):

    model = Manufacturer

    def __init__(self, parent):
        ComboBoxBase.__init__(self, parent)

    def add_items(self):
        self.addItem('', 0)
        for item in self.get_queryset():
            self.addItem(item.full_name(), item.id)

class ComboComponent(ComboBoxBase):

    model = Component

    def __init__(self, parent):
        ComboBoxBase.__init__(self, parent)

    def get_queryset(self):
        return self.get_model().select().order_by(+self.get_model().design)


class ComboDefect(ComboBoxBase):

    model = Defect

    def __init__(self, parent):
        ComboBoxBase.__init__(self, parent)


class ComboPlace(ComboBoxBase):

    model = PlaceDetection

    def __init__(self, parent):
        ComboBoxBase.__init__(self, parent)


class ComboGroup(ComboBoxBase):

    model = GroupDefect

    def __init__(self, parent):
        ComboBoxBase.__init__(self, parent)
