from PyQt5 import QtCore, QtWidgets, QtSql, QtGui

from models import *
from gui.comoboxes import ComboManufacturer
from gui.monitoring_detail import MonitoringFormDialog


class MonitoringForm(QtWidgets.QWidget):

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.initUI()

    def initUI(self):
        main_layout = QtWidgets.QVBoxLayout()

        top_layout = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel("Период с ")
        self.date_start = QtWidgets.QDateEdit()
        self.date_start.setCalendarPopup(True)
        self.date_start.setCurrentSectionIndex(0)
        cur_date = QtCore.QDate().currentDate()
        self.date_start.setDate(
            QtCore.QDate(cur_date.year(), 1, 1))
        self.label_3 = QtWidgets.QLabel(" по ")
        self.date_end = QtWidgets.QDateEdit()
        self.date_end.setCalendarPopup(True)
        self.date_end.setDate(cur_date)
        spacer_top = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        top_layout.addWidget(self.label)
        top_layout.addWidget(self.date_start)
        top_layout.addWidget(self.label_3)
        top_layout.addWidget(self.date_end)
        top_layout.addSpacerItem(spacer_top)
        # top_layout.addItem(spacerItem)

        manufacturer_layout = QtWidgets.QHBoxLayout()
        self.label_2 = QtWidgets.QLabel("Изготовитель")
        self.comboBox = ComboManufacturer(parent=self)
        self.comboBox.setCurrentIndex(0)
        self.btn_filter = QtWidgets.QPushButton("ОК")
        self.btn_filter.clicked.connect(self.apply_filter)
        spacer_man = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        manufacturer_layout.addWidget(self.label_2)
        manufacturer_layout.addWidget(self.comboBox)
        manufacturer_layout.addWidget(self.btn_filter)
        manufacturer_layout.addSpacerItem(spacer_man)

        cp_layout = QtWidgets.QHBoxLayout()
        self.btn_add = QtWidgets.QPushButton("Новая запись")
        self.btn_add.clicked.connect(self.open_form)
        self.btn_edit = QtWidgets.QPushButton('Редактировать запись')
        self.btn_edit.clicked.connect(self.open_form_with_id)
        self.btn_del = QtWidgets.QPushButton("Удалить запись")
        self.btn_del.clicked.connect(self.del_record)
        spacer_cp = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        cp_layout.addWidget(self.btn_add)
        cp_layout.addWidget(self.btn_edit)
        cp_layout.addWidget(self.btn_del)
        cp_layout.addSpacerItem(spacer_cp)

        c_layout = QtWidgets.QVBoxLayout()
        self.tv = TableMonitoring(self)
        self.tv.doubleClicked.connect(self.open_form_with_id)
        c_layout.addWidget(self.tv)

        bot_layout = QtWidgets.QHBoxLayout()
        self.btn_exit = QtWidgets.QPushButton("Выход", parent=self)
        self.btn_exit.clicked.connect(self.parent().closeActiveSubWindow)
        spacer_bot = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        bot_layout.addSpacerItem(spacer_bot)
        bot_layout.addWidget(self.btn_exit)

        main_layout.addLayout(top_layout)
        main_layout.addLayout(manufacturer_layout)
        main_layout.addLayout(cp_layout)
        main_layout.addLayout(c_layout)
        main_layout.addLayout(bot_layout)
        self.setLayout(main_layout)

    def apply_filter(self):
        self.tv.apply_filter()

    def open_form_with_id(self):
        id_record = self.tv.get_id_record()
        self.open_form(id_record=id_record)

    def open_form(self, id_record=0):
        form = MonitoringFormDialog(parent=self, id_record=id_record)
        form.resize(950, 800)
        form.setWindowModality(QtCore.Qt.WindowModal)
        form.show()

    def del_record(self):
        print(self.tv.get_id_record())


class TableMonitoring(QtWidgets.QTableView):
    model = QtGui.QStandardItemModel()

    def __init__(self, parent=None):
        QtWidgets.QTableView.__init__(self, parent)
        self.create_model()
        self.setModel(self.model)
        self.set_pedit_recaram()

    def get_id_record(self):
        row = self.currentIndex().row()
        print(row)
        if row == -1:
            row = 0
        return self.model.item(row, 0).text()

    def set_param(self):
        self.setSelectionBehavior(1)
        self.hideColumn(0)
        self.verticalHeader().hide()
        self.setColumnWidth(1, 250)
        self.setColumnWidth(2, 550)

    def create_model(self):
        self.model.clear()
        self.model.setColumnCount(3)
        self.model.setHeaderData(1, QtCore.Qt.Horizontal, 'Дата документа')
        self.model.setHeaderData(2, QtCore.Qt.Horizontal, 'Изготовитель')

        for elem in self.get_query():
            id = QtGui.QStandardItem(str(elem.id))
            id.setEditable(False)
            date = QtGui.QStandardItem(elem.datedoc.isoformat())
            date.setEditable(False)
            man = QtGui.QStandardItem(elem.manufacturer.name)
            man.setEditable(False)
            self.model.appendRow([id, date, man])

    def get_query(self):
        id = self.parent().comboBox.get_id()
        date_start = self.parent().date_start.date()
        date_end = self.parent().date_end.date()
        if id:
            query = Monitoring.select().where(
                (Monitoring.manufacturer == id)
                & ((Monitoring.datedoc >= date_start.toPyDate())
                & (Monitoring.datedoc <= date_end.toPyDate()))
            )
        else:
            query = Monitoring.select().where(
                (Monitoring.datedoc >= date_start.toPyDate()) & (
                            Monitoring.datedoc <= date_end.toPyDate())
            )
        return query

    def apply_filter(self):
        self.create_model()
        self.set_param()
