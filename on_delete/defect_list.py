from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtWidgets import QInputDialog, QMessageBox

from config import ROLE_ID
from gui.delegate import GroupComboBoxDelegate
from models import *


columns = {'id': 0,
           'name': 1,
           'group': 2}


class ModelDefectList(QtGui.QStandardItemModel):

    def __init__(self, parent):
        QtGui.QStandardItemModel.__init__(self, parent)
        self.on_delete = set()
        self.on_update = set()

    def set_param(self):
        self.setColumnCount(len(columns))
        self.setHeaderData(columns['id'], QtCore.Qt.Horizontal, 'id')
        self.setHeaderData(columns['name'], QtCore.Qt.Horizontal, 'Наименование')
        self.setHeaderData(columns['group'], QtCore.Qt.Horizontal, 'Группа')

    def get_row(self):
        id = QtGui.QStandardItem()
        id.setEditable(False)
        name = QtGui.QStandardItem()
        group = QtGui.QStandardItem()
        return {
            'id': id,
            'name': name,
            'group': group
        }

    def set_empty_row(self):
        row = self.get_row()
        self.appendRow(list(row.values()))

    def set_row(self, defects):
        row = self.get_row()

        id = row['id']
        id.setText(str(defects.id))
        id.setEditable(False)

        name = row['name']
        name.setText(str(defects.name))

        group = row['group']
        group.setText(defects.group.name)
        group.setData(defects.group, ROLE_ID)

        return [id, name, group]

    def set_data(self, defects):
        for elem in defects:
            self.appendRow(self.set_row(elem))

    def delete_rows(self, rows):
        for row in rows:
            id = self.item(row.row(), columns['id']).text()
            self.on_delete.add(id)
        for id in self.on_delete:
            result = self.findItems(id, QtCore.Qt.MatchContains, columns['id'])
            if result:
                result = result[0]
                self.removeRow(result.row())

    def get_id_defect(self, row):
        return self.item(row, columns['id']).text()

    def set_on_update(self, row):
        self.on_update.add(self.get_id_defect(row))

    def new_record(self):
        name, ok_n = QInputDialog.getMultiLineText(self.parent(), '', '', '')
        group, ok_g = QInputDialog.getItem(self.parent(), '', '', GroupDefect().get_list(), 0, False)
        if ok_n and ok_g:
            id = GroupDefect().get(GroupDefect.name == group).id
            new_defect = Defect.create(name=name, group=id)
            self.appendRow(self.set_row(new_defect))

    def save(self):
        # удаляем элементы из базы, которые в модели помечены на удаление
        for id in self.on_delete:
            defect = Defect.get(id=int(id))
            defect.delete_instance()

        # находим элемент по id, если не удален - обновляем
        for id in self.on_update:
            result = self.findItems(id, QtCore.Qt.MatchContains, columns['id'])
            if result:
                result = result[0]
                defect = Defect.get(id=int(id))
                defect.name = self.item(result.row(), columns['name']).text()
                defect.group = self.item(result.row(), columns['group']).data(ROLE_ID)
                defect.save()


class DefectList(QtWidgets.QWidget):

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.initUI()
        self.create_table()

    def initUI(self):
        main_lay = QtWidgets.QVBoxLayout(self)
        cp_lay = QtWidgets.QHBoxLayout()
        self.btn_new_rec = QtWidgets.QPushButton('Новая запись', parent=self)
        self.btn_new_rec.clicked.connect(self.new_rec)
        # self.btn_edit = QtWidgets.QPushButton('Изменить запись', parent=self)
        # self.btn_edit.clicked.connect(self.edit_rec)
        self.btn_del = QtWidgets.QPushButton('Удалить запись', parent=self)
        self.btn_del.clicked.connect(self.del_rec)
        spacer_cp = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        cp_lay.addWidget(self.btn_new_rec)
        # cp_lay.addWidget(self.btn_edit)
        cp_lay.addWidget(self.btn_del)
        cp_lay.addSpacerItem(spacer_cp)

        tbl_lay = QtWidgets.QHBoxLayout()
        self.tv = QtWidgets.QTableView(self)
        self.tv.doubleClicked.connect(self.edit_rec)
        tbl_lay.addWidget(self.tv)

        bot_lay = QtWidgets.QHBoxLayout()
        self.btn_close = QtWidgets.QPushButton('Выход', parent=self)
        self.btn_close.clicked.connect(self.parent().closeActiveSubWindow)
        spacer_bot = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        bot_lay.addSpacerItem(spacer_bot)
        bot_lay.addWidget(self.btn_close)

        main_lay.addLayout(cp_lay)
        main_lay.addLayout(tbl_lay)
        main_lay.addLayout(bot_lay)
        self.setLayout(main_lay)

    def new_rec(self):
        last_index = self.model.rowCount()
        self.model.new_record()
        self.tv.selectRow(last_index - 1)

    def edit_rec(self):
        row = self.tv.currentIndex().row()
        self.model.set_on_update(row)

    def del_rec(self):
        rows = self.tv.selectionModel().selectedRows()
        self.model.delete_rows(rows)

    def create_table(self):
        self.get_data()
        self.model = ModelDefectList(self)
        self.model.clear()
        self.model.set_param()
        self.model.set_data(self.data)
        self.set_param_tv()

    def set_param_tv(self):
        self.tv.setModel(self.model)
        self.tv.setSelectionBehavior(1)
        self.tv.setItemDelegateForColumn(columns['group'], GroupComboBoxDelegate(self))
        self.tv.verticalHeader().hide()
        self.tv.resizeColumnsToContents()
        self.tv.resizeRowsToContents()

    def get_data(self):
        self.data = Defect.select().order_by(+Defect.name)

    def closeEvent(self, event):
        if self.model.on_delete or self.model.on_update:
            user_select = QMessageBox.question(
                self,
                'Выход',
                "Хотите сохранить запись? " +
                "(Save-сохранить, Discard-без сохранения, cancel-не закрывать)",
                QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel,
                QMessageBox.Cancel
            )
            if user_select == QMessageBox.Save:
                self.model.save()
                event.accept()
            if user_select == QMessageBox.Discard:
                event.accept()
            if user_select == QMessageBox.Cancel:
                event.ignore()
        else:
            event.accept()