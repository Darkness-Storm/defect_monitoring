import re
import os

def print_class_attributes(cls):
    print('[class attributes]')
    for attribute in cls.__dict__.keys():
        if attribute[:2] != '__':
            value = getattr(cls, attribute)
            if not callable(value):
                print(attribute, '=', value)

def print_class_attributes1(cls):
    print('[class attributes]')
    for attribute in cls.__dict__.keys():
        if attribute[:2] != '__' and attribute[:1] != '_': #and attribute[-4:] != '_set'
            value = getattr(cls, attribute)
            if not callable(value):
                print(attribute, '=', value)

def field_type(str_search):
    rule = r'\<([A-Za-z]+)\:'
    res = re.search(rule, str_search)
    if res:
        #print(res.group(1))
        return res.group(1)

def start():
    os