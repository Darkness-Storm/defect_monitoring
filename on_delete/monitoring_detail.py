from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import QMessageBox

from gui.comoboxes import ComboManufacturer
from config import ROLE_ID
from gui.delegate import ComponentComboBoxDelegate, DefectComboBoxDelegate, PlaceComboBoxDelegate
from models import Monitoring, DefectiveComponent


columns = {'id': 0,
           'monitoring_id': 1,
           'component': 2,
           'defect': 3,
           'serial': 4,
           'place': 5,
           'returns': 6}


class Model(QtGui.QStandardItemModel):

    def __init__(self, parent):
        QtGui.QStandardItemModel.__init__(self, parent)
        self.on_delete = set()

    def set_param(self):
        self.setColumnCount(len(columns))
        self.setHeaderData(columns['component'], QtCore.Qt.Horizontal, 'Изделие')
        self.setHeaderData(columns['defect'], QtCore.Qt.Horizontal, 'Дефект')
        self.setHeaderData(columns['serial'], QtCore.Qt.Horizontal, 'Номер')
        self.setHeaderData(columns['place'], QtCore.Qt.Horizontal, 'Место')
        self.setHeaderData(columns['returns'], QtCore.Qt.Horizontal, 'Возврат')

    def get_row(self):
        id = QtGui.QStandardItem()
        monitoring_id = QtGui.QStandardItem()
        component = QtGui.QStandardItem()
        defect = QtGui.QStandardItem()
        serial = QtGui.QStandardItem()
        place = QtGui.QStandardItem()
        returns = QtGui.QStandardItem()
        returns.setCheckable(True)
        returns.setTristate(False)
        return {
            'id': id,
            'monitoring_id': monitoring_id,
            'component': component,
            'defect': defect,
            'serial': serial,
            'place': place,
            'returns': returns
        }

    def set_empty_row(self):
        row = self.get_row()
        self.appendRow(list(row.values()))

    def set_row(self, def_component):
        row = self.get_row()

        id = row['id']
        id.setText(str(def_component.id))
        id.setEditable(False)

        monitoring_id = row['monitoring_id']
        monitoring_id.setText(str(def_component.monitoring))
        monitoring_id.setEditable(False)

        component = row['component']
        component.setText(def_component.component.full_name())
        component.setData(def_component.component.id, ROLE_ID)

        defect = row['defect']
        defect.setText(def_component.defect.name)
        defect.setData(def_component.defect.id, ROLE_ID)

        serial = row['serial']
        serial.setText(def_component.serial)

        place = row['place']
        place.setText(def_component.place.name)
        place.setData(def_component.place.id, ROLE_ID)

        returns = row['returns']
        returns.setCheckState(def_component.returns*2)

        return [id, monitoring_id, component, defect, serial, place, returns]

    def set_data(self, def_components):
        for elem in def_components:
            self.appendRow(self.set_row(elem))

    def delete_rows(self, rows):
        for row in rows:
            id = self.item(row.row(), columns['id']).text()
            self.on_delete.add(id)
        for id in self.on_delete:
            result = self.findItems(id, QtCore.Qt.MatchContains, columns['id'])
            if result:
                result = result[0]
                self.takeRow(result.row())

    def print_on_delete(self):
        print('на удаление помечены следующие id:')
        for elem in self.on_delete:
            print(elem)

    def save(self, monitoring_id):
        for index in range(self.rowCount()):
            id = self.item(index, columns['id']).text()
            if id:
                def_comp = DefectiveComponent.get(DefectiveComponent.id==int(id))
            else:
                def_comp = DefectiveComponent()
            def_comp.component = self.item(index, columns['component']).data(ROLE_ID)
            def_comp.monitoring = monitoring_id
            def_comp.defect = self.item(index, columns['defect']).data(ROLE_ID)
            def_comp.place = self.item(index, columns['place']).data(ROLE_ID)
            def_comp.returns = (self.item(index, columns['returns']).checkState())/2
            def_comp.revoke = False #(self.model.item(index, 7).checkState())/2
            def_comp.serial = self.item(index, columns['serial']).text()
            def_comp.save()
        for elem in self.on_delete:
            def_comp = DefectiveComponent.get(DefectiveComponent.id==int(elem))
            def_comp.delete_instance()


class MonitoringFormDialog(QtWidgets.QDialog):

    def __init__(self, parent=None, id_record=0):
        QtWidgets.QDialog.__init__(self, parent)
        self.initUI()
        self.id_record = id_record
        self.create_table()

    def initUI(self):
        self.setWindowTitle('Test')
        main_lay = QtWidgets.QVBoxLayout()

        top_lay = QtWidgets.QHBoxLayout()
        self.lab_date = QtWidgets.QLabel('Дата документа', parent=self)
        self.date_doc = QtWidgets.QDateEdit(parent=self)
        self.date_doc.setCalendarPopup(True)
        self.date_doc.setCurrentSectionIndex(0)
        self.date_doc.setDate(QtCore.QDate.currentDate())
        self.lab_man = QtWidgets.QLabel("Изготовитель", parent=self)
        self.combo_man = ComboManufacturer(parent=self)
        spacer_top = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        top_lay.addWidget(self.lab_date)
        top_lay.addWidget(self.date_doc)
        top_lay.addWidget(self.lab_man)
        top_lay.addWidget(self.combo_man)
        top_lay.addSpacerItem(spacer_top)

        cp_lay = QtWidgets.QHBoxLayout()
        self.btn_new_rec = QtWidgets.QPushButton('Новая запись', parent=self)
        self.btn_new_rec.clicked.connect(self.new_rec_in_table)
        self.btn_edit = QtWidgets.QPushButton('Изменить запись', parent=self)
        self.btn_edit.clicked.connect(self.edit_rec)
        self.btn_del = QtWidgets.QPushButton('Удалить запись', parent=self)
        self.btn_del.clicked.connect(self.del_rec)
        spacer_cp = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        cp_lay.addWidget(self.btn_new_rec)
        cp_lay.addWidget(self.btn_edit)
        cp_lay.addWidget(self.btn_del)
        cp_lay.addSpacerItem(spacer_cp)

        tbl_lay = QtWidgets.QHBoxLayout()
        self.tv = QtWidgets.QTableView(self)
        tbl_lay.addWidget(self.tv)

        bot_lay = QtWidgets.QHBoxLayout()
        self.btn_close = QtWidgets.QPushButton('Выход', parent=self)
        self.btn_close.clicked.connect(self.close)
        spacer_bot = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        bot_lay.addSpacerItem(spacer_bot)
        bot_lay.addWidget(self.btn_close)

        main_lay.addLayout(top_lay)
        main_lay.addLayout(cp_lay)
        main_lay.addLayout(tbl_lay)
        main_lay.addLayout(bot_lay)
        self.setLayout(main_lay)

    def create_table(self):
        self.get_data()
        self.model = Model(self)
        self.model.clear()
        self.model.set_param()
        if self.id_record:
            self.model.set_data(self.data.def_components)
        self.set_param_tv()

    def set_param_tv(self):
        self.tv.setModel(self.model)
        self.tv.setSelectionBehavior(1)
        self.tv.hideColumn(columns['id'])
        self.tv.hideColumn(columns['monitoring_id'])
        self.tv.setItemDelegateForColumn(columns['component'], ComponentComboBoxDelegate(self))
        self.tv.setItemDelegateForColumn(columns['defect'], DefectComboBoxDelegate(self))
        self.tv.setItemDelegateForColumn(columns['place'], PlaceComboBoxDelegate(self))
        self.tv.verticalHeader().hide()
        self.tv.setColumnWidth(columns['component'], 250)
        self.tv.setColumnWidth(columns['defect'], 300)
        self.tv.setColumnWidth(columns['returns'], 65)
        self.tv.resizeRowsToContents()

    def get_data(self):
        if self.id_record:
            self.data = Monitoring.get(Monitoring.id == self.id_record)
            self.date_doc.setDate(self.data.datedoc)
            self.combo_man.setCurrentIndex(
                self.combo_man.findData(self.data.manufacturer.id))
        else:
            self.data = Monitoring()

    def new_rec_in_table(self):
        self.model.set_empty_row()

    def closeEvent(self, event):
        # close = QMessageBox()
        # close.setText("You sure?")
        # close.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        # close = close.exec()
        #
        # if close == QMessageBox.Yes:
        #     event.accept()
        # else:
        #     event.ignore()

        user_select = QMessageBox.question(
            self,
            'Выход',
            "Хотите сохранить запись? " +
            "(Save-сохранить, Discard-без сохранения, cancel-не закрывать)",
            QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel,
            QMessageBox.Cancel
        )
        if user_select == QMessageBox.Save:
            self.save()
            if self.parent().isVisible():
                self.parent().apply_filter()
            event.accept()
        if user_select == QMessageBox.Discard:
            event.accept()
        if user_select == QMessageBox.Cancel:
            event.ignore()

    def save(self):
        self.data.datedoc = self.date_doc.date().toPyDate()
        self.data.manufacturer = self.combo_man.get_id()
        self.data.save()
        self.model.save(self.data.id)

    def edit_rec(self):
        # print(self.model.item(self.tv.currentIndex().row(), columns['id']).text())
        self.model.print_on_delete()

    def del_rec(self):
        rows = self.tv.selectionModel().selectedRows()
        self.model.delete_rows(rows)

