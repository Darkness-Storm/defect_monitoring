import xlsxwriter

# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook('сентябрь.xlsx')
worksheet = workbook.add_worksheet()
# w_sheet = workbook.get_worksheet_by_name('Sheet1')
worksheet.set_column(0, 1, 5)
worksheet.set_column(2, 3, 36)
data = ['Сведения',
        'по дефектам двигателей  производства ПАО "Автодизель" г. Ярославль,',
        'выявленным в процессе производства за сентябрь 2018 г.'
        ]
merge_format = workbook.add_format({
    'bold':     True,
    'border':   0,
    'align':    'center',
    'valign':   'vcenter'
})

for idx, value in enumerate(data):

    worksheet.merge_range(idx, 1, idx, 3, value, merge_format)

# worksheet.write_string(0, 1, 'test record')

# # Some data we want to write to the worksheet.
# expenses = (
#     ['Rent', 100],
#     ['Gas',   100],
#     ['Food',  300],
#     ['Gym',    50],
# )
#
# # Start from the first cell. Rows and columns are zero indexed.
# row = 0
# col = 0
#
# # Iterate over the data and write it out row by row.
# for item, cost in (expenses):
#     worksheet.write(row, col,     item)
#     worksheet.write(row, col + 1, cost)
#     row += 1
#
# # Write a total using a formula.
# worksheet.write(row, 0, 'Total')
# worksheet.write(row, 1, '=SUM(B1:B4)')

workbook.close()