import sys

from PyQt5 import QtWidgets

import config

config.check_config()

app = QtWidgets.QApplication(sys.argv)


from gui.main_window import MyWindow
window = MyWindow()
desktop = QtWidgets.QApplication.desktop()
rect = desktop.availableGeometry()
window.move(5, 5)
window.resize(rect.width()-100, rect.height()-100)
window.show()
sys.exit(app.exec_())
