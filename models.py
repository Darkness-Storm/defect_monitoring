import re
import os

from peewee import *

import config


db = SqliteDatabase(os.path.join(config.basedir, 'defect.sqlite'))


class BaseModel(Model):

    class Meta:
        database = db
    @staticmethod
    def get_instance():
        raise NotImplementedError


class Signer(BaseModel):
    parent_id = IntegerField()
    position = CharField(verbose_name='Должность')
    name = CharField(verbose_name='ФИО')


class GroupDefect(BaseModel):
    name = CharField(verbose_name='Наименование')

    def get_list(self):
        return [elem.name for elem in self.select().order_by(+GroupDefect.name)]

    def __str__(self):
        return self.name

    @staticmethod
    def get_instance():
        return GroupDefect()


class Defect (BaseModel):
    name = CharField(verbose_name='Наименование')
    group = ForeignKeyField(GroupDefect, backref='defects', verbose_name='Группа')

    class Meta:
        order_by = ['name']

    def get_list(self):
        return [elem.name for elem in self.select().order_by(+Defect.name)]

    def __str__(self):
        # return 'model: {2}, id: {0}, name: {1}'.format(str(self.id), self.name, self.__class__.__name__)
        return self.name

    @staticmethod
    def get_instance():
        return Defect()


class Component(BaseModel):
    name = CharField(verbose_name='Наименование')
    design = CharField(verbose_name='Чертеж')

    def full_name(self):
        return '[' + self.design + '] - ' + self.name

    def get_list(self):
        return [elem.full_name() for elem in self.select().order_by(+Component.design)]

    def __str__(self):
        # return 'model: {2}, id: {0}, name: {1}'.format(str(self.id), self.full_name(), self.__class__.__name__)
        return '[' + self.design + '] - ' + self.name

    @staticmethod
    def get_by_fullname(str_search):
        rule = r'\[([^\]]+)\]'
        res = re.search(rule, str_search)
        if res:
            try:
                return Component.get(Component.design == res.group(1))
            except DoesNotExist as e:
                print('DoesNotExist - ' + e.__str__())
                return None
        else:
            return None


class PlaceDetection(BaseModel):
    name = CharField(verbose_name='Наименование')

    def get_list(self):
        return [elem.name for elem in self.select()]

    def __str__(self):
        return self.name


class Manufacturer(BaseModel):
    name = CharField(verbose_name='Наименование')
    legal_form = CharField(verbose_name='Форма')
    adress = CharField(verbose_name='Адрес')

    def full_name(self):
        return self.legal_form + ' "' + self.name + '" ' + self.adress

    def get_list(self):
        return [elem.full_name() for elem in self.select().order_by(+Manufacturer.name)]

    def get_list1(self):
        for elem in self.select():
            return elem.full_name()

    def __str__(self):
        return self.legal_form + ' "' + self.name + '" '

    @staticmethod
    def get_instance():
        return Manufacturer()


class Monitoring(BaseModel):
    datedoc = DateField(verbose_name='Дата')
    manufacturer = ForeignKeyField(Manufacturer, verbose_name='Изготовитель')


class DefectiveComponent(BaseModel):
    monitoring = ForeignKeyField(Monitoring, backref='components')
    component = ForeignKeyField(Component, verbose_name='Компонент')
    defect = ForeignKeyField(Defect, verbose_name='Дефект')
    place = ForeignKeyField(PlaceDetection, verbose_name='Место')
    returns = BooleanField(default=False, verbose_name='Возврат')
    revoke = BooleanField(default=False, verbose_name='Аннулировано')
    serial = CharField(max_length=20, verbose_name='Номер')

    @staticmethod
    def get_instance():
        return DefectiveComponent()


class Purchase(BaseModel):
    datedoc = DateField(verbose_name='Дата')
    manufacturer = ForeignKeyField(Manufacturer, verbose_name='Изготовитель')


class PurchaseComp(BaseModel):
    purchase = ForeignKeyField(Purchase, backref='components')
    component = ForeignKeyField(Component, verbose_name='Компонент')
    number = IntegerField(verbose_name='Количество')

    @staticmethod
    def get_instance():
        return PurchaseComp()


MODELS = [PurchaseComp,
          Purchase,
          DefectiveComponent,
          Monitoring,
          Manufacturer,
          PlaceDetection,
          Component,
          Defect,
          GroupDefect,
          Signer
          ]


def exists_db():
    if not os.path.exists(db.database):
        print('База данных не найдена. Создание новой базы')
        # change_dir = QtWidgets.QFileDialog(parent=self)
        # change_dir.setViewMode(1)
        # change_dir.setFileMode(2)
        # change_dir.setDirectory(QtCore.QDir.currentPath())
        # result = change_dir.exec()
        # if result == QtWidgets.QDialog.Accepted:
        #     print(change_dir.selectedFiles())
        # else:
        #     print("Нажата кнопка Cancel")
        # #
        with db:
            db.create_tables(MODELS)


def create_tables():
    with db:
        db.create_tables(MODELS)